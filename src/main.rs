extern crate libc;
extern crate rand;
mod person;
use person::Person;
mod elevator;
use elevator::Elevator;
mod floor;
use floor::Floor;

use std::{thread, time};
use std::io::Write;
use std::io::stdout;
use std::ffi::CString;

fn main() {
    let mut elev = Elevator::new();
    let max = 9;
    let mut floors = [
        Floor::new(8, max),
        Floor::new(7, max),
        Floor::new(6, max),
        Floor::new(5, max),
        Floor::new(4, max),
        Floor::new(3, max),
        Floor::new(2, max),
        Floor::new(1, max),
        Floor::new(0, max),
    ];

    loop
    {
        // Drawing
        unsafe {
            libc::system(CString::new("cls").unwrap().as_ptr());
            // Why did all of the packages have to be broken?
        }
        //print!("{}", out.join("\n"));
        let mut fup: usize = 0;
        let mut fdn = max;
        for flr in floors.iter_mut() {
            flr.update();
            println!("{}", flr.to_str(&elev));

            // Pick the farthest floor toward the highest bias
            if elev.dir < 0 && flr.calling >= 0 {
                if flr.num > elev.floor {
                    elev.upbias += ((max as i8)-((elev.floor as i8)-(flr.num as i8)).abs()) as usize;
                    if flr.num > fup {
                        fup = flr.num;
                    }
                } else {
                    elev.dnbias += ((max as i8)-((elev.floor as i8)-(flr.num as i8)).abs()) as usize;
                    if flr.num < fdn {
                        fdn = flr.num;
                    }
                }
                //println!("BIAS: {} {}", elev.upbias, elev.dnbias);
            } else if elev.floor == flr.num {
                // Pick up people the way we're going or if we stopped
                if flr.calling >= 0 && ((elev.dir == 0 && flr.calling as usize <= flr.num)
                    || (elev.dir == 1 && flr.calling as usize > flr.num)
                    || elev.to_floor == flr.num) {
                    // TODO: Wait for more people?
                    //println!("ADDED PERSON {} {}", elev.to_floor, flr.num);
                    elev.add(Person::new(flr.calling as usize));
                    flr.serve();
                    /*if elev.to_floor == flr.num {
                        println!("Change to {}", flr.calling);
                        elev.to_floor = flr.calling as usize;
                        if (flr.calling as usize) < flr.num {
                            elev.dir = 0;
                        } else {
                            elev.dir = 1;
                        }
                    }*/
                } else if elev.to_floor == flr.num { // Nobody calling but we stopped
                    elev.to_floor = 0;
                    elev.dir = -1;
                    //println!("Dest!");
                }
            }
        }
        if elev.dir >= 0 {
            elev.update();
        } else if elev.upbias > 0 || elev.dnbias > 0 {
            if elev.upbias > elev.dnbias {
                // Going up will serve more people
                elev.dir = 1;
                elev.to_floor = fup;
                //println!("Going up to {}", fup);
            } else {
                // TODO: Catch case of elevator on same floor?
                elev.dir = 0;
                elev.to_floor = fdn;
                //println!("Going down to {}", fdn);
            }
            elev.upbias = 0;
            elev.dnbias = 0;
        }
        stdout().flush().is_ok();
        thread::sleep(time::Duration::from_secs(1));
    }
}
