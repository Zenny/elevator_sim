pub struct Person {
    pub to_floor: usize,
}

impl Person {
    pub fn new(floor: usize) -> Person {
        Person {
            to_floor: floor
        }
    }
}