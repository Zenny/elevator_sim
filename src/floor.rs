
use rand::prelude::*;
use elevator::Elevator;

#[derive(Clone)]
pub struct Floor {
    pub num: usize,
    tiles: String,
    pub calling: i8,
    max_floors: usize,
}

impl Floor {
    pub fn new(n: usize, m: usize) -> Floor {
        Floor {
            num: n,
            tiles: String::from(" |-]______|"),
            calling: -1,
            max_floors: m
        }
    }

    pub fn to_str(&self, elev: &Elevator) -> String {
        if elev.floor == self.num {
            let mut ret = self.tiles.replace("-", "#");
            if elev.dropped {
                ret = ret.replacen("|", "o", 1);
            }
            return ret;
        }
        self.tiles.clone()
    }

    pub fn update(&mut self) {
        let mut rng = rand::thread_rng();
        let mut i:usize = 0;
        let mut newtiles = self.tiles.clone();
        for c in self.tiles.chars() {
            let dontmove = [']', 'o'];
            let mut next = '_';
            if i > 0 {
                next = self.tiles.chars().nth(i-1).unwrap_or('_');
            }
            if c == 'o' && !dontmove.contains(&next) { // Move person forward
                newtiles.replace_range(i-1..i+1, "o_");
            }
            i += 1;
        }
        self.tiles = newtiles;
        if self.tiles.contains("]o") && self.calling < 0 {
            if rand::random() { // Going up
                if self.num + 1 < self.max_floors { // We can go up
                    self.calling = rng.gen_range(self.num + 1, self.max_floors) as i8;
                } else {
                    self.calling = rng.gen_range(0, self.num) as i8;
                }
            } else {
                if self.num >= 1 { // We can go down
                    self.calling = rng.gen_range(0, self.num) as i8;
                } else {
                    self.calling = rng.gen_range(self.num + 1, self.max_floors) as i8;
                }
            }
        }
        if rng.gen_range(0,35) == 2 { // 0 to 19
            self.tiles.replace_range(8..9, "o");
        }
    }

    pub fn serve(&mut self) {
        self.calling = -1;
        self.tiles = self.tiles.replacen("o", "_", 1); // Remove the person first in line
    }
}