use floor::Floor;
use person::Person;

pub struct Elevator {
    pub floor: usize,
    pub upbias: usize,
    pub dnbias: usize,
    pub dir: i8,
    pub to_floor: usize,
    pub dropped: bool,
    people: Vec<Person>
}

impl Elevator {
    pub fn new() -> Elevator {
        Elevator {
            floor: 0,
            upbias: 0,
            dnbias: 0,
            dir: -1, // 0 down, 1 up, -1 idle
            to_floor: 0,
            dropped: false,
            people: Vec::new(),
        }
    }

    pub fn update(&mut self) {
        self.dropped = false;
        if self.dir == 1 {
            //println!("up!");
            self.floor += 1;
        } else if self.dir == 0 && self.floor != self.to_floor {
            //println!("dn!");
            self.floor -= 1;
        }
        let mut ind: usize = 0;
        let mut rem: usize = 0;
        for ind in 0..self.people.len() {
            if self.people.get(ind-rem).unwrap().to_floor == self.floor {
                //println!("Dropoff {} {}", ind, self.people.len());
                self.people.remove(ind-rem);
                self.dropped = true;
                rem += 1;
            }
        }
    }

    pub fn add(&mut self, p: Person) {
        if (self.dir == 0 && p.to_floor < self.to_floor)
            || (self.dir == 1 && p.to_floor > self.to_floor)
            || self.to_floor == self.floor {
            self.to_floor = p.to_floor;
            if self.to_floor < self.floor {
                self.dir = 0;
            } else {
                self.dir = 1;
            }
            //println!("Change to {}", self.to_floor);
        }
        self.people.push(p);
    }
}